<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Cache\Storage\StorageInterface;


class DoscgController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
        $this->lineToken = "ea44rLXhagB9w0DaqMI7MdH4YF9HHnBqLGAShwuOXz2siiUXeajKHPtUu/uNu7ehT74nlKhYMu4tUgPKzZDqXWIVb+z0NSiHEDvfNMC4ra8Ec/p37Qqerjy66/PQppQscTcHAZvGw3ALivUFUGWITAdB04t89/1O/w1cDnyilFU=";
    }
################################################################################
    public function indexAction() 
    {
        try
        {
            $view = new ViewModel();
            $view->action = $this->params()->fromRoute('action', 'index');
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function xyzAction(){
        $response = $this->getResponse();

        $input = array(5, 9, 15, 23);
        $ans_index = 7;

        $input = array_reverse($input);
        $data = array();
        for ($i=0; $i < $ans_index+count($input) ; $i++) { 
            if($i==0){
                $diff = $input[$i] - $input[$i+1];
                $val = $input[$i];
            }else{
                $val = $val - $diff;
                $diff = $diff - 2;
            }
            
            if(!isset($input[$i])){
                $data[] = $val;
            }
        }

        $result = implode(",",$data);
        $result .= "<br/> x =".$data[0]." y= ".$data[1]." z=".$data[6];

        $response->setContent($result);
        return $this->getResponse();
    }
################################################################################
    public function abcAction(){
        $response = $this->getResponse();
        
        $a = 21;
        $b = 23-$a; //A + B = 23
        $c = -21-$a; //A + C = -21
        $result = "b : ".$b."<br/>";
        $result .= "c : ".$c;

        $response->setContent($result);
        return $this->getResponse();
    }
################################################################################
    public function directionAction(){
        $response = $this->getResponse();
        $gg_url = "https://maps.googleapis.com/maps/api/directions/json";
        $apikey = "AIzaSyC4DEREhnYrzIy87eRLcOm0KbhzcPxk_7o";
        $origin = "SCG Bang Sue";
        $destination = "centralwOrld";
        
        $client = new Client();
        $client->setUri($gg_url);
        $client->setParameterGet(array(
            'origin'  => $origin,
            'destination' => $destination,
            'alternatives' => "true",
            "key" => $apikey
        ));

        $gg_response = $client->send();
        $response->setContent($gg_response->getBody());

        return $this->getResponse();
    }
################################################################################

    public function lineAction(){
        $response = $this->getResponse();
        $content = file_get_contents('php://input');
        $arrayJson = json_decode($content, true);

        if(isset($arrayJson)){
            $message = $arrayJson['events'][0]['message']['text'];

            if($message == "สวัสดี"){
                $line_url = "https://api.line.me/v2/bot/message/reply";
                $arrayPostData['replyToken'] = $arrayJson['events'][0]['replyToken'];
                $arrayPostData['messages'][0]['type'] = "text";
                $arrayPostData['messages'][0]['text'] = "สวัสดีจ้าาา";
                $this->replyMsg($arrayPostData,$line_url);
            }else{
                $pushTxt= "Notification message after 10 second";
                sleep(10);
                $this->pushMsg($arrayJson['events'][0]['source']["userId"],$pushTxt);
            }
        }

        $response->setContent("");
        return $this->getResponse();
    }

    private function pushMsg($userId,$pushTxt){
        $response = $this->getResponse();

        $line_url = "https://api.line.me/v2/bot/message/push";
        $accessToken = $this->lineToken;
        $msg[0] = array(
            "type"=>"text",
            "text"=> $pushTxt
        );
        $postParams = array(
            "to" => $userId,
            "messages" => $msg,
        );
        $this->replyMsg($postParams,$line_url);

        $response->setContent("");
        return $this->getResponse();
    }

    private function replyMsg($arrayPostData,$curl_url){
        $accessToken = $this->lineToken;

        $request = new Request;
        $request->getHeaders()->addHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$accessToken
        ]);
        $request->setUri($curl_url);
        $request->setMethod('POST');
        $request->setContent(json_encode($arrayPostData));

        $client = new Client;
        return $client->send($request);
    }
}