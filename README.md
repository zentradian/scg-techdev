
1) Start from ./module/Application/config/module.config.php

2) For serious configuration, you have to change DB configuration like below
'Db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=dbname;host=db host',   
        'driver_options' => array( 
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'utf8mb4\''
        ),
        'username' => 'username',
        'password' => 'password', 
    ),

3) Make sure your DB is correct and have "users,users_line" table (download here : https://www.dropbox.com/s/l60gtynaq7752vl/zp11926_db.sql?dl=0)
